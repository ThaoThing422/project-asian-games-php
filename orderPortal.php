<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="webStyle.css">
	</head>
	<body>
		<div>
			<header class="headerWeb">
				<div><h1>VOLLITIC</h1></div>
				<div><h3><u>ORDER</u></h3></div>
				<div><script type="text/javascript">document.write(Date())</script></div>
			</header>
			<nav class="menuWeb">
				<button><a href="orderPortal.php">ORDER</a></button>
				<button><a href="shoppingCartPortal.php">SHOPPING CART</a></button>
				<button><a href="paymentPortal.php">PAYMENT</a></button>
				<button><a href="paidTransactionPortal.php">PAID TRANSACTION</a></button>
			</nav>
			<div class="containerWeb">
				<?php
					function printRowBall(){
						include 'connection.php';
						$sql = "SELECT code, image, price, name FROM items";
						$result=mysqli_query($conn,$sql);
						if ($result->num_rows > 0) {
						    // output data of each row
						    while($row=mysqli_fetch_assoc($result)) {
						    	if(substr($row["code"],0,2) == '17'){
						        	echo "
						        	<td>
							        	<form action=\"detailItemPortal.php\" method=\"post\">
							        		<img src=\"".$row["image"]."\">
							        		<ul>
							        			<li><input type=\"submit\" name=\"code\" value=\"".$row["code"]."\"></li>
							        			<li>".$row["name"]."</li>
							        			<li>Rp.".$row["price"].",00</li>
							        		</ul>
							        	</form>
						        	</td>";
								}
						    }
						} else {
						    echo "<font color=\"red\">There is no any items.</font>";
						}
						$conn->close();
					}
					function printRowShoes(){
						include 'connection.php';
						$sql = "SELECT code, image, price, name FROM items";
						$result=mysqli_query($conn,$sql);
						if ($result->num_rows > 0) {
						    // output data of each row
						    while($row=mysqli_fetch_assoc($result)) {
						    	if(substr($row["code"],0,2) == '18'){
						        	echo "
						        	<td>
						        		<form action=\"detailItemPortal.php\" method=\"post\">
							        		<img src=\"".$row["image"]."\">
							        		<ul>
							        			<li><input type=\"submit\" name=\"code\" value=\"".$row["code"]."\"></li>
							        			<li>".$row["name"]."</li>
							        			<li>Rp.".$row["price"].",00</li>
							        		</ul>
							        	</form>
						        	</td>";
						    	}
						    }
						} else {
						    echo "<font color=\"red\">There is no any items.</font>";
						}
						$conn->close();
					}
				?>
				<table>
					<tr><?php printRowBall(); ?></tr>
					<tr><?php printRowShoes(); ?></tr>
				</table>
			</div>
		</div>
	</body>
</html>