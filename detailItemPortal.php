<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="webStyle.css">
	</head>
	<body>
		<div>
			<header class="headerWeb">
				<div><h1>VOLLITIC</h1></div>
				<div><h3><u>DETAIL ITEM</u></h3></div>
				<div><script type="text/javascript">document.write(Date())</script></div>
			</header>
			<nav class="menuWeb">
				<button><a href="orderPortal.php">ORDER</a></button>
				<button><a href="shoppingCartPortal.php">SHOPPING CART</a></button>
				<button><a href="paymentPortal.php">PAYMENT</a></button>
				<button><a href="paidTransactionPortal.php">PAID TRANSACTION</a></button>
			</nav>
			<div class="containerWeb">
				<form action="handleOrder.php" method="post">
					<?php 
						$code = $_POST["code"]; 
						include 'connection.php';
						$sql = "SELECT code, image, price, quantity, name FROM items WHERE code = '".$code."'";
						$result=mysqli_query($conn,$sql);
					?>
					<table>
						<?php
						if ($result->num_rows > 0) {
						    // output data of each row
						    while($row=mysqli_fetch_assoc($result)) {
						    	echo "
						    	<tr>
						    		<td><img src=\"".$row["image"]."\"></td>
					        	</tr>
					        	<tr>
					        		<td><b>ID</b></td>
					        		<td>".$row["code"]."</td>
					        		<input type=\"hidden\" name=\"code\" value=\"".$row["code"]."\">
					        	</tr>
					        	<tr>
					        		<td><b>Name</b></td>
					        		<td>".$row["name"]."</td>
					        	</tr>
					        	<tr>
					        		<td><b>Price per Item</b></td>
					        		<td>Rp.".$row["price"].",00</td>
					        	</tr>
					        	<tr>
					        		<td><b>Total Quantity</b></td>
					        		<td>".$row["quantity"]."</td>
					        	</tr>
					        	<tr>
					        		<td><b>Order Quantity</b></td>
					        		<td><input type=\"text\" name=\"orderQuantity\"></td>
					        	</tr>
					        	";
						    }
						} else {
						    echo "<font color=\"red\">There is no any items.</font>";
						}
						?>
					    <tr>
							<td><button><a href="orderPortal.php">Back</a></button></td>
							<td><input type="submit"></td>
						</tr>
					</table>
					<?php $conn->close(); ?>
				</form>
			</div>
		</div>
	</body>
</html>