<?php
$paymentDate = date("d-m-Y");
$email = $_POST["email"];
$phone = $_POST["phone"];
$address = $_POST["address"];
$totalItems = $_POST["totalItems"];
$totalPrice = $_POST["totalPrice"];
$account = $_POST["account"];
$bankName = $_POST["bankName"];

//echo $totalItems."<br>".$totalPrice."<br>".$email."<br>".$phone."<br>".$address."<br>";

include 'connection.php';
$sqlSelect = "SELECT o.code, i.price, o.order_quantity 
FROM `order` o JOIN items i
WHERE o.code = i.code";

$result=mysqli_query($conn,$sqlSelect);
while($row=mysqli_fetch_assoc($result)) {
	$sqlInsert = "INSERT INTO paid_transaction (payment_date, code, price_per_item, order_quantity, email, phone, address, total_item, total_price, account, bank_name) 
	VALUES ('".$paymentDate."', ".$row["code"].", ".$row["price"].", ".$row["order_quantity"].", '".$email."', '".$phone."', '".$address."', ".$totalItems.", ".$totalPrice.", ".$account.", '".$bankName."')";
	if(mysqli_query($conn,$sqlInsert)){
		//Delete list of order after payment.
		$sqlDeleteAllFromOrder = "DELETE FROM `order`";
		if(mysqli_query($conn,$sqlDeleteAllFromOrder)){
			header('Location: paymentPortal.php');
		}
	}	
}

mysqli_close($conn);
?>