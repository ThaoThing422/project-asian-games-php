<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="webStyle.css">
	</head>
	<body>
		<div>
			<header class="headerWeb">
				<div><h1>VOLLITIC</h1></div>
				<div><h3><u>SHOPPING CART</u></h3></div>
				<div><script type="text/javascript">document.write(Date())</script></div>
			</header>
			<nav class="menuWeb">
				<button><a href="orderPortal.php">ORDER</a></button>
				<button><a href="shoppingCartPortal.php">SHOPPING CART</a></button>
				<button><a href="paymentPortal.php">PAYMENT</a></button>
				<button><a href="paidTransactionPortal.php">PAID TRANSACTION</a></button>				
			</nav>
			<div class="containerWeb">
				<?php 
					include 'connection.php';
					$sql = "SELECT o.code, o.order_date, i.name, i.image, i.price, i.quantity, o.order_quantity 
					FROM `order` o JOIN items i 
					WHERE o.code = i.code";
					$result=mysqli_query($conn,$sql);
				?>
				<table>
					<?php
					if ($result->num_rows > 0) {
					    // output data of each row
					    while($row=mysqli_fetch_assoc($result)) {
					    	echo "
					    	<form action=\"detailOrderPortal.php\" method=\"post\">
						    	<tr>
						    		<td><img src=\"".$row["image"]."\"></td>
					        	</tr>
					        	<tr>
					        		<td><b>ID</b></td>
					        		<td><input type=\"submit\" name=\"code\" value=\"".$row["code"]."\"></td>
					        		<td><b>Order Date</b></td>
					        		<td>".$row["order_date"]."</td>
					        		<td><b>Name</b></td>
					        		<td>".$row["name"]."</td>
					        	</tr>
					        	<tr>
					        		<td><b>Price per Item</b></td>
					        		<td>Rp.".$row["price"].",00</td>
					        		<td><b>Order Quantity</b></td>
					        		<td>".$row["order_quantity"]."</td>
								</tr>
					        	<tr>
					        		<td><b>Sub Total</b></td>
					        		<td>Rp.".$row["price"]*$row["order_quantity"].",00</td>
					        		<td><b>Total Quantity</b></td>
					        		<td>".$row["quantity"]."</td>
					        	</tr>
					        </form>
				        	";
					    }
					} else {
					    echo "<font color=\"red\">There is no any order.</font>";
					}
					?>
				</table>
				<?php $conn->close(); ?>
			</div>
		</div>
	</body>
</html>